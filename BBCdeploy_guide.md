## BBC data import (quick) guide



### Process overview
The steps in this guide will let you move data from json/csv files to relational DBMS like PostgreSQL. A pack of sql scripts restructure the data and construct a database of the data model concept. The whole process is divided in phases:

A. Pre-configuration (sort of)
B. Data import
C. Mapping & flattening
D. Restructuring / Normalizing / Build

#### Process steps
Those phases are technically a sequence of the following steps:

##### phase A
- [joining multiple data files](#joining-multiple-files-into-one) - collections into one file _(optional)_
- [transferring](#locating-data) the data file on server _(optional ; admin access required)_

##### phase B
- [creating a database](#database-setup) with spatial extensions
- importing - supported for **json** and **csv** formats
  - _(I)_ from a [json file](#importing-json-data--i) into a _(jsonb type)_ PostgreSQL table
  - _(II)_ from a [csv file](#importing-from-csv--ii) into a flat PostgreSQL table

##### phase C
 - [mapping and flattening](#flattening--mapping) data in a PostgreSQL table

##### phase D
- [preparing](#prepare-a-session) a session _(optional)_
- [building](#building-the-database) the database - **creating and populating tables**

> Database tables are built according to the [data model][bbc data model].

![data conditioning flow](data-conditioning_final.png "data conditioning flow")
_Add comments or modify this schematic representation of the import process by [opening][data conditioning] it editing mode._



### Joining multiple files into one
This rather easy step can be done with a _cross-platform_ command  _`cat`_ available via the console. Pressing <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>T</kbd> on _Linux_ systems will open terminal window. 

Type  

    $ cat /path/to/data_one.json /path/to/data_two.json > /path/to/data_all.json

**`head`** and **`tail`** commands will help you verify contents in the concatenated file.

`$ head -5 /path/to/data_all.json`  
`$ tail -5 /path/to/data_all.json`

Those will display first five and last five lines on _stdout_. _(without numeric argument both commands will output 10 lines)_.

>All files with one entry on each line can be processed whether it is a json, csv or any other type.

Count the total number of lines with  
`$ wc -l /path/to/data_all.json`

>On a Windows system the _path_ should be prefixed with a drive letter, eg. **`C:/`** and the character __`\`__ should be used insted of **`/`** as folder name separator.



### Locating data
Transferring multiple GB of data to a desired location can be sped-up with _Common Internet File System (CIFS)_ protocol.

If necessary install _`cifs`_ using the apt command  
`$ sudo apt-get install cifs-utils`

Become _**root**_  
`$ sudo -i`

Chose a location (create a folder)  
`# mkdir /mnt/BBC`

Then mount the repository  
`# mount -t cifs //blalacar.racing.fast/garage /mnt/BBC -o username=<user>`

Copy the contents  
`# cp -a /mnt/BBC/garage/bbc/data/db_bbc_clean.csv /home/<user>/`

Then un-mount the point  
`# umount -l /mnt/BBC`

Exit the root session  
`# exit`

The last thing to do is modifying the owner and permissions of the transferred file.  
`$ sudo chown <user> db_bbc_clean.csv`  
`$ sudo chmod 664 db_bbc_clean.csv`



### Database setup
PostgreSQL should be installed on your system as well as the spatial component PostGIS prior taking further action. You should also be able to access it either as **_postgres_** either as a **_user_** with privileges to create and manage databases.

Enter the psql  
`$ sudo -u postgres psql`

and type  
`=# CREATE DATABASE bbcdataYY_MM;`

Spatial data requires a spatial extension which has to be created in the database where spatial data operations will take place.

Let's connect to a _spatial_ DB  
`=# \c bbcdataYY_MM`

and  
`=# CREATE EXTENSION postgis;`  
`=# CREATE EXTENSION postgis_topology;`

> A clean-up script [bbc_drop_types-tables.sql](bbc_drop_types-tables.sql) is available to delete previously constructed tables and types. It can be used if the script was executed on a non-appropriate database by error or simply to clean everything and restart the process from zero.


### Importing json data _(I)_
> Skip to [csv part](#importing-from-csv--ii) part to process csv text files. 

Table **`t`** will be used as an intermediate table to _inject_ json records. The table name as well as its column name __`j`__ should be equal to those defined in the mapping script _(see the [flattening & mapping section](#flattening--mapping))_. If different table or column names are supposed to be used in this step then they have to be modified accordingly in the mentioned script below.  
`=# CREATE TABLE t (j jsonb);`

The 'injection' is done with a simple transfer from a file into the **`t`** table _(mind the table name again)_.  
`=# \COPY t FROM '/path/to/data12.json' CSV QUOTE e'\x01' DELIMITER e'\x02'`

Using the `head` argument will limit number of entries. This is useful during test phases.  

    =# \COPY t FROM PROGRAM 'head -100000 /path/to/data12.json' CSV QUOTE e'\x01' DELIMITER e'\x02'

The _csv mode_ allows to define the escape characters. That assures correct data parsing. It is recommended to verify special characters, role of quotes, points, commas and the encoding of the original data file.

Any modification in json file should be avoided and should never be considered as an approach of managing data since changing large amounts of unreadable data will lead to undesirable results, even more if multiple datasets have to be handled. If at any point parsing starts failing the data treatment strategy has failed with it.



### Importing from csv _(II)_
Building database from csv makes the whole process simpler and faster since the 'parsing' step is not needed. An init table called **`bbcinit`** has to be set up as an import target table. This is handled with the script [bbc_init4csv.sql](bbc_init4csv.sql).

To initiate the process run  
`=# \i ./bbc/bbc_init4csv.sql`

List existing tables with the _psql meta command_ to verify if the init table was created.  
`=# \dt`
```sql
               List of relations
  Schema  |      Name       | Type  |  Owner
----------+-----------------+-------+----------
 public   | bbcinit         | table | postgres
 public   | spatial_ref_sys | table | postgres
 topology | layer           | table | postgres
 topology | topology        | table | postgres
(4 rows)
```
Then the import can start.  
`=# \COPY bbcinit FROM ./bbc/data/db_bbc_clean.csv CSV HEADER DELIMITER ';'`



### Flattening & mapping
Flattening json data into a large relational table is implemented by a sql script [mapjson2flat.sql](mapjson2flat.sql).  

The script does the following:

- defines data types
- maps the **`keys`** from json file to table __`columns`__
- extracts data form json
- flattens data into a table respecting the json structure

To initiate the process run  
`=# \i ./bbc/mapjson2flat.sql`



### Prepare a session
Populating the database can take a certain amount of time. During that period a computer might switch to stand-by mode and interrupt currently ongoing session. The the _`screen`_ command will prevent that, keeping the initiated processes alive.

You can also [jump](#building-the-database) straight to the building part and skip this additional precautionary step which is more an energy saving measure than anything else.

---
To verify if current _\<user\>_ has any previous sessions left list all current screen sessions:  
`$ screen -ls`

If there are none create a new screen session with  
`$ screen`

To resume a detached session type  
`$ screen -r <session>`

If the session is active and you would like to reattach it use  
`$ screen -d -r <session>`  
<sub>[more][screen] about screen</sub>



### Building the database  
Build is the final step. While connected to the appropriate database run the script to build and populate the database.

- Run [bbc_build_all.sql](bbc_build_all.sql) if the data origin is a **json** file.  
`=# \i './bbc/bbc_build_all.sql'`

- and [bbc_csv_build.sql](bbc_csv_build.sql) if the source is a **csv** file.  
`=# \i ./bbc/bbc_csv_build_new.sql`

The build will create new tables and relations based on the [data model][bbc data model]. If this is a server session you are now able to explore the data by using [pgAdmin][pgadmin] or via [psql].

Keep reading if your database is on a local computer and you would like to transfer it on a server.



### Moving data to a remote location
These instructions might be helpful to transfer the data on a server.

First export the database on your local system  
`$ sudo -u postgres pg_dump bbcdataYY_MM > /path/to/bbc_io.sql`

Then copy the file to the wanted location _(server access will be password protected)_.  

    $ scp /path/to/bbc_io.sql driver@blalacar.racing.fast:/remote/location/destination/folder

>Don't forget to create a database before importing data, see the [database setup](#database-setup) section above.

Now login to the server where the data will be held and connect to the spatial database.  
`$ sudo -u postgres psql`  
`=# \c bbcdb`

and import the whole database  
`=# \i /remote/location/destination/folder/bbc_io.sql`

Or execute the import directly from the console.  

    $ sudo -u postgres psql bbcdb < /remote/location/destination/folder/bbc_io.sql



### References
Sources and references used for writing sql scripts mentioned in this manual can be found in comments within these same scripts. See below for some SQL related topics. There is also an [exhaustive archive of bookmarks](bbc_bmarks_20_01.html) at your disposal _(the section exceptional_bkmrks is exceptional, indeed)_. 

- computing time intervals
<sup>https://stackoverflow.com/questions/7796657/using-a-variable-period-in-an-interval-in-postgres#7797994
http://www.postgresqltutorial.com/postgresql-interval/
https://stackoverflow.com/questions/9376350/postgresql-how-to-concat-interval-value-2-days</sup>
- computing geometries
<sup>http://postgis.net/docs/using_postgis_dbmanagement.html
https://gis.stackexchange.com/questions/8699/creating-spatial-tables-with-postgis</sup>
- handling arrays and strings
<sup>https://stackoverflow.com/questions/11188756/how-to-find-the-size-of-an-array-in-postgresql#30139522</sup>
- managing json data
<sup>https://stackoverflow.com/questions/27181980/how-to-convert-postgresql-9-4-jsonb-to-object-without-function-server-side-langu/27183115#27183115
https://stackoverflow.com/questions/58992119/importing-json-data-and-flattening-records-populating-tables-with-postgresql
https://dbfiddle.uk/?rdbms=postgres_11&fiddle=980af21fb4ef4a75e2941763fea0b5d5</sup>
- managing database types and tables
<sup>https://dba.stackexchange.com/questions/88988/postgres-error-column-must-appear-in-the-group-by-clause-or-be-used-in-an-aggre
https://stackoverflow.com/questions/7624919/check-if-a-user-defined-type-already-exists-in-postgresql#41705872</sup>
- database performance issues
<sup>https://stackoverflow.com/questions/32208692/postgres-create-column-and-update-column-values-with-query-output#32208922
</sup>




[psql]: https://www.postgresql.org/docs/current/tutorial.html
[pgadmin]: https://www.pgadmin.org/
[screen]: https://www.computerhope.com/unix/screen.htm#Command-Line-Options
[bbc data model]: https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1#R7Z1tk5u4soB%2Fy%2F3gutlTNS7EOx8zk5ndnN3M5m6yb%2BeLi7HxDBtsHIwzmfPrrwQIA2psbBAIj5ytHRvbgKXWo1Z3q3ui3ay%2B%2Fxi5m6cP4cILJqqy%2BD7R3k1UVUWWhv%2BQIy%2FpEaSo2ZHHyF9kx%2FYHPvn%2F9bKDZnZ05y%2B8bemDcRgGsb8pH5yH67U3j0vH3CgKn8sfW4ZB%2Baob9zG7orI%2F8GnuBh7zsT%2F9RfxE787Q92%2F85PmPT%2FTSyHTSdx7c%2BZfHKNytswuuw7WXvrNy6Xmya26f3EX4XDik3U60mygM4%2FTZ6vuNF5CGpU12v9A%2FGV9%2Fvrv%2Fe%2Fd1%2B%2B%2FlX2%2BfnT%2Bu0pPdnfKV%2FNdF3jru9tRZ%2F31zg13WlHM%2FfpnNw9Vm68XZ745faDtvn%2F1V4OIW0q6X4Tr%2BlL2j4NfzJz9Y%2FOK%2BhDtyi9sYtyt9df0URv5%2F8efdAL%2BF8AH8dhRnYqSa5Gx%2BENyEQRgl19FuFfKv9M1P5IzZtSJvi7%2F7kbYHqhz64H4vffAXdxvTuwyDwN1s%2FYfkvskXV2706K%2BvwzgOV9mH6K%2B8K9%2FUMnng993Af1zjY3N8LS%2BibZH%2BGkR%2BTSJP3oKejsoNefEYuNttfjMrf04%2FFEfhF69wNSV5ZCeHjgfugxdc5%2BIL3WhDucnk65sXxd73woDK5OhHL1x5cfSCP0Lf1RU7%2FU4ODMVKDzzvh59qZNL1VBh5tpqN%2BmzEP%2BYnz6%2F3GyaEu37EPbS%2FoFa9INL1qcFcEiENuKRRvqQb4F5bu7F3TdptWxw0%2BEnh9%2B4PJUPphGFlMcPKX8zIyGJH1JO7IU9xJ8W%2BG%2Bx%2Fu3Ydh5tMOAJvSQU4yn4Xef5AhRYVhDL97DXpTB8D8m12eOUvFslZiyMtgR2WvY0799ePv6RX0fT9od%2Byq5FDIT7lMkgE%2BQmfzFsngh67sfuQQ2AT%2Bus4aWbjGv%2BHgXSj4G4y8C%2B9wa%2FR%2FjX%2Bj3w8wqK9xrLv%2BomEenikPntktF4vonDzGY9Nj%2F7aInAMYNDxHydm43GSSalqNhsUmlI%2FKEqieaoc2owcfvy5tQg%2BFFkJiFNTSSyLXWdSd0ywyM%2FMxkz%2FImTViFCuGmU%2Fq6RilESrIEtUbHoRJYfVFGZrd%2BXxIZpyXI6Sk40cZ2Ljy2iIL4q5zmUOKYzQDU2vVOpeI7qcGvHpDF38xIhlFyNG1fZ8ild0seKtF2%2FJGrVAmERZpytNtV3zJ2sh5gL4GFl%2BZGKCb6H4KnhIPkxlk1lxeN%2F9%2BK%2FC87%2FJ86mRvXr3vfDWu5f8EripyZfQVMHKdnYg%2FaaFV8vZgf2Xk1elb3%2F0Ih%2F3GFkSJQdPW35sw100947PnTGlaN3nVCsTLm9BrQY1nCuuHBRAIvODkRe4sf%2BtbG04sIT5SIZ1Yfmiamp5%2BaKZlXk6%2FfXZ94qr%2BOqpNKd8KlupnCltH%2BZMWMLcl8LHMvJwWPyoLLVpF%2B%2FHG2ZIXB5myVr9IBopmjONgWEzRXuTUdeUn01luD0ds29dKVNHMVCpj%2BkQOVcM6UfC5ZJadbruc8T0uTKd4oNmQHrqIcLPHsmzIJzjlTc53bpTmchtMfCEPWaJUKa2ZVplfnSCJTrhUikzphVLCUd5UTufkguz6MJ3V%2BF68fnJX3OZrbO5GDEzc8muWZqJ89n3b%2FrF4zNx9qV8Hj4yB3c83SJVbzrhGpZoEy6ik1Am26ZtnzvfovJ8a9JmOTLfdjZSNGak4JemuyJYWz9syZ%2BFt%2FTX7Eq8DU9rlj8DTrE5MroAqmNrwk%2BpOqtGTaf%2FYqdU%2FOu3%2FpbcVLjsRc8aakrtTgKwkmWpml6e%2FlTRJcIyme4lxKVG8Wwm3C8FSW%2BHUfwUPoZrN%2FglTMwh5OA%2FXhy%2FZB3j7uKwOrmeN%2F%2Bwk3J1cqxzccEzdBPJaWatqaymoRV3x5On5TScOzWqJx6dO1tOiZqtT5FVFnnD1qb0To9MjJ3JMOsaa7I4bOpTOA63%2FrFl1Rlez8GWgZSK1i4%2BtlgvVDqRNe71uonolXS7MjUVGq8iRjcvP%2F54%2F8%2FjX5H%2Bl69%2FeW%2BvZn%2FoeBXQ%2B%2Byk5geSVdUUIf2S56hejKzQxAV2t9b1vNXYxg%2FeDhvJdEnzyqEB1wlgNK1TC1PFcNkDfQC94sImGb4yQHQLg66KRyAEoIuygVXx2AzQHA4puPfzUWmttF861cxEVUFg5xdRli2Q3JkN4V9Yb9uGURIurRtTn1Gxz2mGWj5FU0ufgdDUKTwqt6sb%2BlQvvF2xJ9ZYAfvUsx3WUshOgVWInSruZyCuoYzSwQtz65Tw1LbuMnqRSjwCx56rM%2FWJbMw7sVu70VIUvV3X8u9LBNhtA3%2F9BR95E%2Fiuvw3XPzAd21dcvaeQf5OxxtXTE%2FxCvB3Z%2FV14qL1jlc09yGSj3lXq3Cq60nQ6L54Yaa8Y5QvqtsJcENlQZL9Wvl4PYfZIZ6c8fzGLI%2FcfaPOKDLXvKNR%2B0BGU87VVGD40ZLjFTiOdnRPuZCD%2BkNGse3SMLxQf5av%2BEvaolvHr9b9vbz6%2FfweoGZKAr5OAUCR%2FvwQE7LNyK9KwBOzAdtgDAb8u%2FPnPPynX6Lft38%2FX1%2FebP%2F5vCW0M2XjRCi8N1vHMX7BWP7kpaZhNSYBsNeVbLczshjDrYj8JLH1ssPPQJLvYbUmN4VQWrAPUOJli%2FOSIdVf429nDzg84LV4lwIQAGKIdPxzBWLuJJNgYCKYKRjB2IRpGi0huCr9ofGn20PhiF5MSX2PAlyEYvoCkFsRpJPl10fwy9YH5BeyQlvwaA78csfilsvx6d%2Ftxtkh3WMf%2BCjcv2SgoaXbBNHNY%2F3y%2FNNMkzUZJM3U4mtVIOouzt7%2F9JnEmKs64xGqgHq378E1LnvXHswYiNOK8YxrrKFr4mGHrOQHaduUGAY0Xlzy7UJ7pPdr6a6SQdTNJng3GM60uBHgMPGPdRotd5MZ%2BuCbfJVHb%2BK3kqS%2BxdtFYM3v0AdQIIxtTK7E2HNa4h9Pys1%2FUU00tYU3yTAie8bCiqc7QPgGJs%2F5w1qUVLaWHSEoaG5Ox8DazJPFqqqjhaz2SQlCcqpFIpAmhomlqj46BGlGUcRoiqWjc4%2F05ShKb3sWNIgm1Vwc1fXj3AJvOTkJtOKhxr6jEUZJYf2eQ5HGWMHsdMLMG9w3o0tcpEsxG7OvU2WWnhNfg8GqazxTxBZ3T0FuAdG7iKZeiAoFO574U5ShJFiM4B%2FMck6Y7J89xXjnGKpaOUSaHSsc0qSXH1qjZZ8QMN946q5pTKTm3XhS%2FwmDl7g7EinqCeFVGS1NRyjqjs4T89IMD5DWGxc1mxO082QKqBhbEraNChed3%2F7G8rOdWpfhnt9pk1zchQa4Mho7FTlPthnKHaKJE3oUgdDuvnE5DyHVNq5b%2BOprPlGZNreSnM%2FQKeGvSr%2FZXjhDp7DK723qEHdVKqssmdEbSzZwanRRL0qnGN5pyhMgAalB2nVH1WFKhAWoOdtftSYUks8yJq5Y1s17AL3CUApMd%2BXM3YvV%2BmX21INa12VcvPdWq7lQq6yALcDbQvIGlxQadu0%2FLtWra5fHlAFu58ujd4vXMSvLxPlKtWixR%2FcUsGU7KG%2Fxn6i9UmXGQX8bBHgaMWWd%2BPBAG2myA8MsraLFbLGRewWGNOzkphM4rWHPvKiNOK%2FeLJ03Zw5uy%2BVEMyo4KUYyf88SS%2BbhEAphaI0Hiu%2BF0IFBqFS68QBLskglmqwMTTJdRUQIRTB9zVJTFemrneGn7GEYvEmKXDDGkAFECIMUOWFtayh47e0qKDaeHta%2BgfIxi%2FCSJnQ%2Fn4WqJ25ocvb3%2F%2FYNk2SWzTGNNuX2vKVnnh2TZcCxr75kbTCOzWdN%2FxrIZcXltyXsyj82FA214I5kty24IBDSbu5WfoySxVn4Jr0uGV1P7GOLmWLKlhV8keHG38HOUJJ0RnPbB5jS4Nn%2BRhNbuI21rgmvbxps%2FBOH8y1kB5zfJI3%2BnFHFcE%2BB0ZiB6Hog%2FKUThT2lMPhyH31QOs548FkWc11suRhEfLCklSvS6ze4KI2VKZ%2FMEC7s5sGiQwXiTBsF4r7AUOtLoIjqb1oFS6GBlcqQcgHF9eN64SqHbrNskqwl8aLDJ8LxLKAhs122nOyVwD5RkfpFWNutqkYF7A2vF3P0tHMWJ9bfkJdFFgh7ul1vz7i5VXUdHPmjFPzLyAXbMvsnHOmbuJPmGJR937ww%2FcXJY7wwf5EljphAAA8qfw6sebhkQHOmIEYhdDndHDEdJYh0xD2H4BfcYK1DSLjSRmzRVEzmV7AU2VM6T7vkv%2BRM1u16O681AmqGWrmeZebKEIoIpFEv7NCsDpw87kMM6mvBCCEujWOug8ekFw%2B7NzEnZbm8mMCw4aqZsSntp4hlYWeBepqMLcfr2vPnTuVb%2F%2FOnre%2B27%2F%2Bk%2BmP%2FHvEKs1rmJ%2FLms1T2exc7pEAOjzqC5vYNYIVjqWA11aIK9irXOYQQIHXMG3zqrlmEdfOVvt2n6%2Bje4g%2BekDVKkKVf0%2BezZj59m%2B89yyrEheScE78BAtV55J8sQCcS7URRVg2%2BdDfyZ7yL8e%2BfkruT%2BpwvnGLyXs1eQyYTPAoFsFKWHwIhKh41TmScNIfElBr7OKgeZEu3A9s2mNmVuYifzaYyyHGSKC4H2OjmsIrb13Hg7S3pZQkwMiHHRwUxoDzoniNXctFTCRDL%2Bj0ILq7l3Vg3LIgVmJL9Ze7GSJIPEkDzu7gYnGRDz1DfJpD4mEslGnN%2FMYcN%2FKcnil410Z14yyFTqThwMZKrCIVxYguxsERpvxUdV0RjB4VII7czCT233ptdUoTq8LT2XkfO2pdsHpI5vfTQVNS2QpipiVUhTFdZl%2BcsLcapXhTMI%2FM2W9Onzkx97nzBdyDvPkbspS5y73eCBh18s%2Fe9EcusGfC5UtfGzNV3%2BGLkL3yt1c7%2Fi0Dze9kpTysWfDEsFwl9B8jjcetzohTySICcRRDG75kL21UrJuau8dCwVSdtuWL6uqyjr%2FLcWmPMjbseQTNaSO11wp1KMyVTR8NjpqfLrQMl4XhmuaFHDBgqPLpjCw9qibp7c1YOXnVzCpy18TKs8wew3HQ0HH9Zs9AG3khf4K6Ckoez2c7pdtSoF%2BXRl8H6nieR6W2XLNTa%2FNTYa65QDbL65CaZ3XhS564WkTyf0Mai%2Fns46ijE8fVjjcI%2F0kSpvx%2FzRRssfdvvMp3h6G%2BOxvZYr7o74Qwc7XXJrAmg%2FbLJb6WMYLX%2FU8eo%2FcB5bRjjxWIvLopXkATnonWy6Yb2B27FpPzfs2Hz4tXBPUrooU7ycKi%2BvrjKon2sPph8Jl8utx8fOS3eRyNXX%2BOmD7NHShw1%2BTOmjkBPjXxT5m05RlMUQicWi9tlJCywyK8rOlSo%2Bi1iz7zv%2FH%2Bno7sr4p5c9TqphDq%2F%2BNqjhNnhv58ervZ2dSLSOrkY0WDa0zqFjv9TRvFIl5WeW65xL0DSUxq5FOsD7iJmYaqpj6UhBSMsjaOhSX0FT3VBNxzbw%2Bw6lDD1%2F%2BouZaArgKqYyZV0ourN%2FVFLw8Y7TUFm7tQRo65nSQKU%2B1hVge0a%2F%2BOzbTC3xKcRCjUoZf3xq%2BlRVFAvpqqGotlUeAKptTLH6qJqWaSmqiR%2Fn8tOuZjcthFv2xkzW1v4abF2UIe3Xl8pUocWXz5U7uiqtfIPj6lJtkPhJzpQnzpT5FiE6mg0oeLrnubKf4GkZxdjPUqP5XNm5UbNmEjPNqalolmYgpOPlBt0mRIeAZk01PEM6tmI4hlkxnTSfKS19ul9VWLpRuQotL9XXpKmzC423uWVW8bfJCXCf%2B0sfjypVeSAtvfDI7rNdRBI%2Bxv6K%2FEnCaZQwWnhR8rmt93XnrZOUkOES%2F4%2FMxNtpg7m4LOOUj%2FV78%2FKM8rVzMsT8MiY6nLWLKbfV%2FNRJu1R%2BQG%2Fw1gwgnwayKNCL9Da4bbozAFdAOMfjFbAD91UywXHu7pK9tbJkgoglExy7HM1s0ejmohybUAks84ASkl0OrJtpVvZuWNDeDWQBpnSrstbpoWKCbgC%2BfWg0yXIJoymXsMdkq3IJ4KDgpZjrBrunTJZLGHR7%2Fh4NQpdLqBkCrLsY6%2FUSa68La1ABhV6xphqsD1tibVCs7dEwPqzpwAKIFPqtXQMNlj1pMs5Kv6IDDUoFBwGNW94bHZhYh8bZ682gpNdKkFAZlMCs6KzHZe0mRsDEmicOycanpXUjgkWFr2lm8TOIhqCkcLyQBgoiawAZviD5q0DaQSwIDTRY%2BE0Wae5iEXnbraSaKFTjCDINKPrSq26mmizKJMgGW2eao0AZXMeB9efOcXPgU8%2Fm4YIoabKGlUA4O6cITEa4epxBhcV54QwWQlXSbJRFYGrmyaFgBsT2ffj4%2Fp3UyV4BxBwgaKVXiKlSJRslxFJoCLS4pPEiRcv%2Feut5VCGTLBOIZVxCNNDQtn%2FVYpcFEmaDrS9zIoxvfamywT4pzKQLQDyY8VDMVL1Hqz8sgqwnXbJsDIqZKRbK7AZJKiS5BHBecsGYObTNXzfZ%2FappRNAM3LYqI4JEkUcuawSnR6NtjTyy1jbpTh9ulbDng9CrhBoPGrtMeIPPvg93VH68%2FfXDhCaC%2B%2FPn6x8m5G40RBpx4UW%2B%2F82N892D6dbO5HubfCukhGN7OOrdwVHlB0dNbWgM1rlFg5ty0SEOGvd44YZGfpJksamhWyctKHSBym6ixi1McrskW12PZRwoJxLQmvdJ453%2FdKsk9%2F381Xx5mtlzEi8L8iCZQZyO0XVJBsyvuzDOevJqm3Ql2Yevos33%2FZv42WP2N6AfTkSe9vP%2Bg3TYFQ6de7Vf0pStSS6A%2BInMyV880j9e4K285AaSOTp9a4GZQgrbk9b5H3qfD9H5V%2F%2BcZiJ4wp2mmu6KIG%2F9sN0UWgF3TtoQ5cbBh9NWLh%2BubfmsFcmdRI8Pb5REOcE9rxSe%2FTBJ8nDg%2Fze6962HabRwiZSSrwdHbg36Ff10cRro6K8I8d3kglEYeNMjLUy6Fbzbo3eBbHIXWVM%2B5FPCVaUPkKLmbV98rhrGD6Wf3bB50pu9%2FZYUHCvluHCDbUj%2BTNJMFfiXK%2Fch%2BSle9uGgMAjc7MtN2qftEGdOXZk62ifQOLbTrE36jLu7LMNCdYKq6Ah3dyQbypkK5RkVI9RysjKEdGCjv%2B5orF6AkFE%2FEbVUDNg1E5WCHe1%2FIoazeaKT7eZxMiByOck%2FdEV5maVuwb2aCA35Spq0JXkTPDl0vglN7%2BKTK%2BIpsxbF%2BVci6DxefuNJiphlhJVfOm%2FMwyjythuMS6y6knH33d%2FGydPylZI8MvSG7sJoP47JzEPG7ecnL%2F2N6f%2FJhbznRJ1wt%2Fj6pI%2BfiDDMsbxgvTYb29v8G%2Btk3G%2Fw3aSTW%2FKR9Cavr2%2ByCz2423T41pFwF1SPBD494ibCkJ5y67mkOYtT6NxNuPHkRmlTLNNf%2BeTG6WjAHboLks8sIqIeqcpVoTcfanrzodCbz2kLhfP5bpOm9Xnz%2FORlvzPy%2FnebzXr05hJE0Nvz1%2FiGVnQ5%2FoA7deWRD%2B3W7jfXDxLFGwux7%2BbfePvxPWnd%2BTydZq7yltyEgT9%2F%2BaFwl%2FtGYputeLJS1qH8zMsozTqUAnvlx8ldbhueH%2F8Ud7fNbQ7bMLlESJqECCTWmsnfN7s1Jk2SDunwjRPJLwjBqLjNI%2B1Rgvns47zIjrRKLbKrfCFXNB3QbL4lsNMFffdgt1nD6haL5m7LWhB6ynxkJo%2FJK8p81EDWD2TXy24Yr4Toz2qZtfNkydb1Su1wk5VrZAKGDITsernOLgclQ0I0AwPVkRQgjRjSdfaK1I%2FQYy4k1WaX2vz2OcisIQcGQIdWvpybbZIhgYOCX9YQG1Limwth2dsoc4hwEKoRp0ayWSfEYnKjTd46SZuK4xQbH%2BlEJxuQDwkkGz%2BXri3Th4iEsVGkD6m5dzbwzo0i%2F1uKMrYsruTY5XDMVofmGN1YJjkmBMecGgkaAcccNoBTsuuC2YUUNmYYtrlwWwE4rClRwmsweDncIzQ5ShJbTqV1GFIjI%2BvxMimnxSipJ%2FS9992PC3XQ8Ku8DBp%2Bvq%2FsQl7Qwi4NhaFx%2FBPlztHKJ1RV4h0oZStln4lu6GeUPmROayhli7WqaFq1JFhN2RTc%2B%2B5L4WMZE2p%2FgaGWL6VntVv2AyQ9Y7c2Z4c19aHp9F%2BsTzh2H5mx1qYIWW3JE0ryUz17h4ZMYxa2t6vty1w7SrlQHR1dAle5dljbXCoNHXb8wRl4gNJz3fW5MrUdUy33udquz7%2BDX%2BApAaxZYxU%2B%2BIEfkx%2F6hlpqA3eexE3%2BwCpuPXl%2FPYX8m4zV%2Bwt4Yy%2B%2BFA71s9KxoRmsHSU%2FVopj185z%2FyoV969uA95fG9iCYWvl6%2FXg%2FdUU1qTjL2Z4vfCPJwtH8HMBDzmE9rRt4xwGhww3%2F51GtRpZUkKUJfweHeNzB2uKCikcs4W3mS2SzZPC2CLHvUmyL2vSyUQDnMIg0biZwDWFgwlc2iPPhplaI0HiO1PyzXpVmPl4NZPUkZUwu2yYAZ7hvmEmM36KBLNRJGGvuXfWNJzAbBfJal%2BvAmaQq7hvmsmMFSLRjHvGCn6JoSygdrvv%2BmQ35Wz9QL4uVEae8dnYhKeZxiYn65dmuiWjj8Wh2Z4IQtMMzPqJdNZZEJAAF5m7WCiOnZX0M0VbPceATZPcOFYjfTIR%2ByiTF2fYEEcr02j1zeKu8ixfg9THxOEYF30MKCbd8%2BoSScO%2FOPrYngXj08dYbSyh2GwpOSYUx3joYypQQbpnfUxSbIzamGAMq9HEZivJsMtnGFA8ul%2BGSU1slAxTxWIYkKMsYdiju5EUu3yKATWj%2B6WYjL0YJcUEK7aqm6wc%2BdvZPFwtcWOTNx7CMJAoEwRlXML8gcrRPTsrgeo3EmbD1VHinvGNoySxoRfffPfx0Q9nUbh1Jc8un2caUD26b57J4AuReDaK4Iuae2f3SC8jz3tO0j1IlF06yoDa0X3HkcnsbyKhbLzZ3zTEStJyX39CUowDxfIyQcNSDCgd3Xf0hfRbikOxPQnGRzFdZReYczfC31GVN%2FgJkKRGps94nekzNKBCeb%2F5M3SVXYcOX6H8VefP2PNjhPkzEIdkqlJxE3f5qasNCUYFg4PISTenOPDaA4Bf%2Fl5%2BktTAx1Rt0kLSSTbHbikz78J3V%2BF68fnJX%2BeaSjXh7pmdU8z2m91JKbdvIXehkryNm6uQs5e8zJP2khf7rL3JK05pe3NZOZq2F1HKHM3bW0ylp0DCQw%2B2TYGJbH2qFB7l6rcmchrm5QXPXD6XZU2dk3MHd5YIEAFJWNl8vKQMrb%2BlhcsrY6ZNftZs8u8mQWsHSXn3kOgiKa%2BlapUslGo78eSfk1VDkOfqnMrqqeQU8zufXpm7daLffKnRXpRu35rXSQXQ%2FkUJS5JeyQ0ufHpnDbGOq687L3qZLbzY9QG3lczlW5Dn2ly%2BDUrH15tJTpvim2fbVS21JJ0WkDlUpbN8acI%2BlGy3nbqnst4GfzE7LILSUteRpa4xI7vISQtKFj8jiMq6EmRO2gPL0qayoHJ3CnCUCZWRieUuCGZx5G9mcwxquRmgIzsZP7IAuWFBsvBzUqpyY1LnUFFrxEB8T6Omstb2GLenpMqYqAIkae2bKtKC3jlVxpFpdfnxx%2Ft%2FHv%2BK9L98%2Fct7ezX7Q7%2FS2TkmXxbhK7hyUTTCRRGUPJXfqggUK3aukoEGp5Hm4Ggd45qItSQH%2FvpLDWWk9W%2FSwPr3Cit5GbROIGWdkzvfSqIN0Y5Gz19yKS%2BVtbPjCT0dafjwm9BfyJjEy4xJ3CO2VUkvcOhwmxY01i4vzafDRvWodQrnCLQMDUi1Hs6TohHbWRyScjibINksJ5MVCxGtWFbvuIEOqvQFgY6f3UWTe4AFYpw2jj3A0CKQ9S2RYoWzeeQ%2BB1jxx2TzV7iJ3dVG4u214A2q%2FcUNb5BQSlfVMHA7BAih0VaTJJ4Nyk64FocLj6TRE7AAmCRbTrZzEutlsDvJls0NbTVCKQuBjTK1XkYTcTQ31imS2OWWURpJLVekooKNh8oG1gTrVWeTgQDi6GzjjSBgNbYEalsvWEqmvTamQfXBemWaVNXEYdooKrbWaI4cjLWSYmNZckLVwXpecrJefMmxUSw5a0RrsKhxDYgH2SYh4xPhEoaSbbXm3d3NzWuEW%2BttmueEf0D1w%2Fp1iwLVhCXphnOLjjefqH4s8o08Eyz0baTAu6D4N6j4WK8BcDoQACdD5QfOyTeOADjQjsgadZPNPLhD3%2Bx39UgMSgyWw4D7xCAot6wZWVJwWI8E96o%2FHOdUNicHVgUjbysc9%2FZZeiT3BuCeM7z6p0rwCab%2B5fQYH%2Fk0nUNhA%2Bm44GfbK2eGRVxpl3%2FlGO34Jd3VWdoNzbpXbO3TuYOOnyQZNiM4JF8wVU6yvi%2FvhsYd8RQ%2Bhms3%2BCVMJIm0%2Bz9eHL9kDe%2Fu4nByJM1zNd1ys5GtntDD3nc%2FThM2646avSYZm9FUMbKX%2B5zN5MVLfjP7zND56%2Brt4y989CIfdwPZUv7uBAdD01TPKi3fWkng3CD3s1VTZbtlAudqQkqdJq0%2FklcZyNeM7KmjmQp9oNJ5EdLsKaqcuyYXNO6XZB8i%2FVgGjtrfQC5dvpjqmAdvNk%2F5ce4XNEurDNT0prvdsW6wdntlOqXCdigfMJMfAWb78bTBA2SWNuqm73MySztmWb7bJpam8uBMtXLqgyukT41KVgOOSYNN1jaeZpCWotFINLBk0JQxLUVBM6ZOf93ODn4uc3s2z6LCHJsVSaidYvfVFCbFWgr70go11RQG0SPWi2IhiCbawWmawNHpPK9uxGgCR5UIZNWIdktBtipEUytL8Y5maxtVwakZB%2Bfe6hcMq4epl04b5XIPosJVRR3A1Wy%2F5tnPu7ZmVbq55cTbB1zZrbpdzal0JX7pPa9MLUOrpKESv991potbz6DQFAJOM%2BWCRPlU%2BBCE8y9ZjaTyfMjMXSdMjzXZ1JpOmxV70TlawmnTaGkuPBBI32HlpLYFkarc0xtOo8yZHKe8cjGUyuqCd8Ujkw2YSFIpRy%2BzebjwmDHTV2ZCM3lMxpqZ8Iw0hO3s6GwmxJbzxcmZCVG1KI9lZnNEMQ0rTURUspKaB%2FxB2fWgxISokpgQKQaQmVAHLPxmxS7bR2ZCkw3xuL3%2F%2FQM7wmQywo688T24EMy6KJEDsUbNBgU%2FH6nZbpNgsdVRnVBKB3wboeK%2Bc5CjcAEltn8jY11Vbn5K%2F95%2BSv9e3yZ%2FeeBPeueFYB1UQwZiHb%2FtNabcSCgS2Npb9gfbSGgBiXo%2FpyD79Pv79Mn77MCP15Jsl002qI5Nv2SzOES6SbKdK0E5HcZINtYL8i5VzZT7X9K%2FH95Kml0wzcCCOT3jTGZQFQlnao0ICYWzb8%2BbP51r9c%2Bfvr7Xvvuf7oP5f8wrVuG%2F%2BfX3%2B8%2B%2F%2FT37%2Ff7n%2B1%2F%2FvJcku2SSAVkFuZEMlD8kEzoMArKDMBAaYzUINhmxORgERhq4RRAY9eGeFASGJsIHgTUbGp2Hhh2N78qNvEe92rmzqjuvdkvJZP0H%2BFpmQNr3IcLPHsmz2H1kBLg%2BpuZg9wgZpWq1N9LvA6l0m%2B5NahmZcKVNbaQUHrSINvU%2BTVXTKT4q%2FOIYgGOxnoETA69qhIRb2JVgcqJMbduobGnoRGhQfzLAaudzN5rNw9USz12sntRTsIn%2BjvybyGCT0QSbVLfqXCFgcydY8xdZryHYxGZXITLYZOzBJlbdcuakYBNuhbBhQWznppDBJryFyubuueAoXKzn4sHd4kkucVqQp193QAirtPcJau87HXBQhAkEOH52Gls6LkSi2SgcFzX3ztZHyhZGyT0nTMM%2FYn9Acu1iuQbFl%2FTMNVkeSSSujaNAEnzv7MaHdRitXJFS70uCdU0wMKakZ4TJakgiIWwU9ZBq7p31XQS77ztyaqKV4edSHbtomEFhJT3DTO5kEAlmI97J4HCIUJLwEhheQFVK2DXGzVDryM0KAsHL4W7y5yhJrOBwCYsbMO%2FpKZlWOgtvOzexW0OZaxwWZ8PBbk2yv1g1tQdbRscYlVwv1VQvvDO0OEDCqrbhdmNMC0lHficBdyYyywmsrlqGUvUQPOWw%2FgHhM5cJ1vOYZ4qtja7jgRxNuwi32vyF1aJkfiZI4GXInKrRosqZ4Ds6a9PIlwplZc4%2BOhrGHzHnsF4KGTE39og5p4v0TOCg4BfU5Mj0TIIvn0eRngmuz64Atr9bdWKrk7cyYYkoRkBAsJqi7pRAOZBrXZida0RP2gB7g1hjNJUl6xAzRHJgsN7Y299%2Fk%2FgSBF881DIgHo4bvmruWfpfRdLBxut%2F1RWHERxxyrv07%2FZo2OWNPQgOsLEeztSRUajjbPH1pVJqs8CbFIg57SoWl%2B58DGBLqKxq1trFkM2m9WtHLqbmhGl%2BuJ6c5J3Kh2QnvgVLqxa%2FyWaKtuXIlKmql%2Fb2l67CZRs3KC4au4jsZic%2FR%2B9T%2F5KgTE3dKe9S7kYOrqwp5QV%2F54NuQfsAUjSQNqZwWO6CYBZH%2FmaWlI9g6UGP4Lsofo8eXvjfSsJjft2F2fv7Z6VvYC1nXT1WPPN6t3rw8OWVcIn%2FR%2B6PdE44n%2B82Pp5X8QSB7xZzWXmjkCt7LoG0QiTzhyP3ig9DF8eHk19Bjx4dDeXZNJtwW1jjnp%2F82PuElT9yxefI3TBqxJE5mTzu7vYaZHafNcMtu7v03TBaeFGf7gzdKUPWUu2pwS4SIPcCzW7XvWpniazata7cV1%2BuqIfMTXVljc6o8seUZOpYBUWqwaqg8CdpHKkouZ00VhPEZ3Zncz8GfM3SwCKmgeVkmO5Ts2c0NXUgZpQKaz8GF00FFA9pcBnI4KLVyZRYBheofByC9lUHAb5lslBkRKqnABpPIf8mFx5AMyDTNMWYls0qUCA8aEWmAWenBbyYTpmhWJlmLwcxtJqRquNwF3BMsKs6fzE7NCxk2EtHYS%2BAvB%2BklpihK%2BAtsyFUH38ees4WOFalqSTUecuEijcBbx0wLBcZoyQ2Ebms6GRZwY0rfYaOwFIkVwJNkKI2FwJ1vNq8yiHfliQIX4I0jd7oZNsgKDTsElASpB1BtBohEGsPKXTrOhtg%2FZMX%2BbH76OWGVtxuRRcW4Jgi7Xa1TRruLf4Asjff631Vl3aO9Mhum%2FjRnn3cjaST0%2Fd3518i8NdfCt603f7Sirsml%2FITH908jCJvuwnXC%2BKj6vCntbz1GTGVw%2Fd%2F000DEZflwQbq6pfME3Ds5jF8NTEkcvsU7gLyqx%2B8FDtbf%2BFFiUy620JjPLS6zewIhvpVHF4Rthc%2FRRvnodAVwiHk85Mb%2Fy9pkSc8ieBV15OXBChFpNn81SbwVsTMlwywdfo%2B%2Fv%2BCzC%2Fulk1sJJoj256M2pGNLKoKZdqSrrOZ0zQNqmGl81Ky6R0Uer2107rWE1v11hY9s428z4wL%2BARndI39%2B0wndZ%2BO%2BdPkseS0hrrcyBS6ks8alI0sxqHjsEm2KoldiZ4yqEeQniP9TUxUJHsmhzlTZYNphzkcDjVZQeP88faezFmJq2QoF9Qdfox0D7d%2BlOZNKYA4ol1VlAraLRbt4ErYOOCBakV2gzWffPp4eyNl8dJlEVmqPZwsgjvLNBaL%2FtofrgzUyF3yQI6JM9Jc1MvqIPtaT9Sdr%2FLEdscClwztvKwWSiWrhW4DQQU2oK3TvDN8vPzgAKNBXIUB9uv1v29vPr9%2Fxw4y6eDvyME%2F3Ag6QNlTogbA8cItJ4HO%2BojvZNTASDaH64IluNBZR8%2FGi1Z4VlzHM%2FxN6SkUI4SZA8igUlC8IpZh2ZMVU8bJMP0shvHLlAJA7CEMv%2BAOm63CBVFGZdzUZbMMKv%2FUK8uQhNk4YYbEo5nKSJK%2F3Zf8VjDaAs%2FlFHEucSYCzsBaUD3zTJbpHCnPVLF4ZrCbHBYeac1d5M0WbpyoZ%2F4Kt7K72kioXTDUoJpQvULNkPXtRsm0FCHiIM1k3QRuFPn4pQTaawIaVCeqV6CZMkXsKIFmDrdHCL4fQEfbRW626dAn%2FvNvsvzwRbPMHtobYErlbJwsE0w5y%2Bs9FrZQ%2B%2FHLbO2upCfg8kGmoqFdATa7OpAgGwHIrBrBGgpkNqvcu4tF5JGgY4mxS8eYNrQHgLpUJcbGhTG7JgJ4MIyxjqQk43H0MpvL4IxXwTJzaMO%2FLdNZjJNlmmAsY4N8pMH%2FFYHMGdrgb7OGWgmyMYCsptjgYCBjja0rz4tJxGzSAUnov1TMLpxned2d4XjGViaUPBsDzyyxeIZobafiPqbIn3tT3MyEaulxibSLRxqNnh4uclaRkbOjZFrGEJGgxmr7czeaSt3sNYDMHNoBgBQZkTFSkAkWkpFvZykuN8OFF0iOXT7HnKGN%2FygvUSo5NjKO2YJxDLGhPSv3i1xXXj7GdHVo0z9CMth%2FnBhDgkX7IxoiWVpXxt5jmNyWRNmFo0wf2uqPkDSRjRRlopnIwFqlNFOGJNmFk4wWph%2BQZDLtz0hJJlraH6AYaEay2fphRlIxT5KtmBJoFww0Z3CjP5JG%2F5ECTTCjv8UhVFGiq63gnV5ZgUfm2aY555HOSzgl5UZJOes8yPGTI7buFynzRLPdZz3fuhRYn2WyGlUQO6lk2HmlweoqjTHFy4CKaFDVtOKX0k6rFOQ6szYY%2BDk1WxYWi4OdlA%2B%2BbS0wle5Cp1sXqkVpGtcC0ymsqUNXMaZG%2BVw11cBwL7gvhY9lSKq%2FadMplwpRsyJ%2BtfdW%2FcIVynYb7sd0ehP7Ed5BlRCE2HCGhY%2BFbD1niz%2F2VYvHTB6TsdbiOaPwTjtth639U8elRmrQObV4NAeVRxZi1SKwwJRTGcsNK%2FGgSiUepBhAKR6aTL14QbMyf%2FZQigchNmrj9v73D3zWELIOz1AWkQymBzYPNxsT3CqkIJUN%2BjhFCIutjjitHWT1nabCdl6OF47CxQZ17NZ%2BLP3wwlhM%2BKENKMADoo2fsZeSTJpBRsaxutqlg3mvVA5%2BUMkucdkFFNwB2YX4zZwyFcJI2XVeLgSOkmQxgsPTiEsttc2MuIuKLZO%2FHTY3HKPJSYbjSXdG1cMWgaJRFT6jVjM9nmZVrbdfHrCWVmwspoqq1tLGllfDKVtedV1taHntzAijspZO3GpmQIDzEOFnj%2BRZ7D4yIyhZvZTkn8Izm9brF7GJCfIgBxuAqyrtPEBWkwQhO%2F%2BVMrXyDBt5ue30ZUvz%2FhUNoKWnrYhFuFxuPU4SwZrl0HT6rxP6%2F%2BDkeSF9r0xNvTJ6W%2FZ8D12rsYaufRUA6daQbo3z3Bp4SqTTlnRsLKlyIh0bl%2B7Y0Gpc%2B8I4NrR21j%2Fp2BBoUV234hnMsaGxW7ykY%2BNSjINH0Da8Y0Njt4W1ljhpHOyDY4JlfEaadGy8KnYN79jQpGNjpOwSzbGhScfGKBwb1CJwNFq8bnbswa9BkwHSWFWrcpXGXo1qZLquVeZu3j4NTfo0jiHjlfk0NOnTaND3gvs0QAQDxUC%2F7rzoZYZP7OLjb4gbALeWH65%2FYJWsnnwcnkL%2BTS7cx9FYJ2s6rzKyXyu5qmWUxdayAI9ELrkl5cw5oJ3VuyRMupGfTpfUHFZcVkA7Xq3K5bp1SIDNCdQr9Rez4jCRjglOjomGAp9T7LjE1zsXQPHuwv4LCxXr5Pr4c2tJulxvQkNJyMdq03l%2FEI%2FA%2FfLjj%2Ff%2FPP4V6X%2F5%2Bpf39mr2h34FKFkYM%2Fv5V2JmfJgBDP38MAMJlc7OXXcSMydh5uBYHR9mgIhFosTMvuFulImauzXgc%2BOK3ZArXbh9YCmSKeObIEVtjpR8XHJDSifCACpdKoyU%2BGUjiTISouQ5r3tBCihFMudxC6QcHJdiIwVUsFi9deNFPj65FwnEkomq3Zp3dzc3EigMUDQgVWevOgoQRyyBAgOF8Sbf3NzdOc4JCyLudhd%2BYsJqLwQ1%2BPwSNKMAjclmheoZNFJz6XgxlI9JkXCCX0ZhGBedS5G7efqQFOrTbv8f

[data conditioning]: https://app.diagrams.net?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=data-conditioning_final.drawio#R7V1bk5s4Fv41%2FbJV7UIIBDym092ZTSW12emt2WTeaFu2mWDwAO5259ePBBIgJATYYHsmdlIVS%2BAjkL7znYsuuYHvN%2FsPib9df44XOLwxjcX%2BBt7fmCawXED%2BoTVvRY3reEXFKgkW7Kaq4in4gVmlyWp3wQKnwo1ZHIdZsBUr53EU4Xkm1PlJEr%2BKty3jUGx166%2BwVPE090O59v%2FBIluzWmRb1YVfcLBa86YBYi%2F47M%2B%2Fr5J4F7EGozjCxZWNz%2BUYRUW69hfxa60KPtzA90kcZ8W3zf49Dmm%2F8i4rfvfYcrV85gRHWZ8f%2FP7bh%2Fjh33O8yxa%2FZs8PH73dZ3BrsTd88cMd64wbE4VE4N0yJnLJY2dvrJvQn7uYX7hN80F8R24gz7CvLpJvK%2Frvc%2BiTv%2Bu5n1Bc%2BJlfDN8iyII4CqIVHaaQdAdrjDx20V7xa9ZjZdMm6bwt%2FbrbhI%2BJvyFf717XQYaftv6c1r8SYJK6dbYJSQmQr2mWxN%2Fx%2BziMk1wEfHx0DcMor%2FCBhvSNgjCs3XmX%2FyH1q8RfBKR7%2BbV8dIsOYCAGiD4JF2W79Bk4UAh6SGOvwSb0I%2FxYa6Iuhr2iYjRZ1QtOMryvVbHR%2FYDjDc6SN3ILu2p5TJ2YFgIbFeXXCtMIMTSua3BGJkOBz%2FRoVcqu8ES%2BMEgNgZfdCq9060dKeOXjepsWA0sBFsXJxg9liDFUFdAwjRC%2FUFoq8VQ00IKnXuDRQGSZfxRYoOXafUb%2BkQZbB88W0GmA5IfBKiLFEC%2Fpy1LEBITc3rHqLN4W%2FT0nevcpv%2BceWHkHpmu8YA%2B0Cv2UDn%2BuIZyqaGEeb4I5%2B54zXf4ToxL5Pyo%2FrxgBxAiIIHaAPUOuhGPbUuHYmkFzIiSjViBvlSgOgwjf8oejKAaUnm0ZxUPpFiBKt0VvG711hbfy3L8JhQh%2FQ5GUC7ovlI%2BoRYaTnM%2FZVaE50tXPzUeQqJ49WTL920%2FbxwkmFOTnLRTUNPzF2h68pc9I9baV4ipdBX2tpYbwFjZ2F5aC8EQiY4ykpaAJ2cOzzJlR%2B4hcYkIgEQng5CIQiT0RizhTs8gA%2FT5AiWr6%2F56IxPtsR2xP6lOfrj8B%2FP00nV2XR0OptCscEUqcU4c3xH7h7B5PBQm%2BJZZ4u8tq8i6AFnzsLueH0sImWCxCLDHD9ETgyZ6xkgicqYgAKIjgmIFatoQXxYiY6rBHYGBbGAaoG611nAQ%2FiHSft05iPRx%2BidM8uCN1czJwOKmJ%2BNS4oRTFXdfyFycwAj3HHkzmS6pi7sbg42jxjiY2quGsD3ZtZOmw4X2QfaW6M6MvW5S%2F5WUEWPF%2Bz3QrL7zVCl8IV5HXop1f1EXkFb%2BylvJCXRQtV7Ly0lu91JTWOpxpvEvmuNvhzvxkhbPu%2B%2FBCSPHI4KgHD4rYgdcRJ46YsxcxMaRCAGvhSxzkloZhz7V12LNsNLPrl5Eov%2BgUJrKeymm0AoCphbhhiHKLTpTk5uAtu%2BhwPKuC%2FDHwfEsADaAAaOjCIxDN9aNCNXTs08La6Qlr56Jg7QH3%2FLC2GrrVbGZilLsSytdZRhPU72hT5mOSZrtFEM%2BIl5ZXNDSAesttfpHCekuGsdUaqzwDMU%2FT9M0k%2B53Emc8ss8d%2FoEpijWCcgQGB1jMzZrZsn11LhrZptKP4KPPsSQP9OY5WMam6PyiGaYtRgoODFqsWtOTXXmtBSVtoFMXpn2F35BD0iByMoZEDUqCwMyNapuGFPCUtfPEzohRRXkO6Yxxk2haaucCrPiIwbVOGJQev4DY2je94MYOhsLMDwn7TJrDJx%2FrIoLlV0L9mf6R5JqADY89TYEwzYaMm3YbXsQz29wRbXRa%2BP6Ac5M1cs5XpkCMBylFYcGcyOPWIQbVuW%2BlXGTOz9KK%2BCU5Ui0vF4xXBtStdP7VnJw42deNiGq1mb3z8DvfJuGJ1OmX8xgvxypDragBmw8asXl83zIE6sZZrndML49NDY6B2BNBeMTsUs0iLWetQzAItZr3zYhaOh1lnBNA6V9ReUduN2mPTlG2ZRLMz5VLmM0uf4BsXc0XtFbVa1B6bjGxDbX%2FQXjF7xewwzKoWF42B2e7kdgla54raI1Dr2Fp4OXp4%2FTNArFrbMgTE9fwgL6uS0CqftmOaU60eHbpxhblMzh7QwhyNA3NL24qpb2VqmMvTQDqYP4fx%2FLsua9lEPspxuKCrfEvALkoQwhr%2BQQ3%2FnYkItnx%2BGeI9e7Q71lF8ZvNJXJxXjFXLRfLTrKHIeR176kKX%2BD4PxK%2Bycj7BJE94Sen%2FUuFbkrpVUDzMUePi7oMEz9msVxQntIOFtc7V1o7%2F7DK69IvVC7MRh3OA9%2FekAAipcgoaCU0wit4DNFj0xMrOdXvI9oUTTroY81JbqkoIoectl6rJmXn60j03o902ccrpGXGhIX2rx0fZCEuzOCNvqvE83XS1BdX4PNecjjks0zihdRoUBl%2BwdSLQs0z6pw2nLZiuYOqeZsEQH%2BkLsRMAupbOi0MjLRkyvWOamdp%2ByCnUL3GarRL89N9P9aUkp1wHUgxsHOUL2%2B%2FvPj%2F13YNJXjjYprib%2BP10W%2BzSXQZ7aixk365kc8UykZaNdj33Yo7N%2F47l6PBleF38D3geXVgkAqx2hTrOAgzLf05pAf4h8UmnBaioHto9wpWBeeXDLQcf%2BW7TYV2W6QC2VuccaxzTYSDjiGamNh3tG07rG6pqO7IedySGxcSY8O32eJ8lfh7hpsM2Z4k1i%2BCFvjZbdlrZE7YyVbIvSxoqiztDWyST9nPhx7XH3nbDTqWIt1mwqfakNZqmKzgJPinfiPuohAdpjXp6b5lSRjgaM3dnO9C7V3BtI9yRlgUfvvWmazPWr0yxa3s17%2BIsizdiXbV%2FU7m9k%2B03H2t3uGvPbNS2RtMmviCyJAtsQpevKa6zlYdmELVz01FWGKqWap7UCl%2ByRT0046fL3AlZuaPXeA0IXg%2B3z71nAS7MPrtQl583UcO17WuPXe3kAjThOc0x3zl%2BVeiDFPrYXH3Tlz5gCqQ3eZxAocFF6TMwPe1CdcuaNbZB93axLaCTDG37rDqtSpY2XOx3kR%2B%2BpUHaa%2FNC7wRJ37OpRsiHjOB02dDUzsfaM6%2F%2BkR0wABXgdafKgcNhCxGuGZDRMyCHpjpaIH04IXPHqZOQORVcCCPblDZbt6LZzbClLx3rxSLDEBX5tGR8MesqfkKtdUUfTQyaOs99IDrDM%2B7Un4pXdG7hoaq9w%2BFz%2Fko84zCJqvc9L%2BLCVN1Buvkrx7Z1Otl7RZW4E9ERFd8z4MwarPgEp%2F5b7bYtvSHVcJrn6MjHMvkxfxWxFE2MSjPWNTFzzsRMq35fiDYCw3RnfIlfqYPOGDoIDJeIdioxciv8FKITmVzrmtMY0XwOtWgXgnjXhQ0cVgowOINnQmE2HEiSh5uZ0cBuSmBP%2FFcJ7%2ByUlq75HM2BLS2H8B5xXEsrhIacuuIhHflY8lkEqgN2JztyxZJTMcVhh6RTfpIhcj3QOSYePOWYjLihQzrWy%2FCEKIu%2BWa8dHDREtcSsAqGwrgil7VCw8Y716h1%2B8Bsvhf4dYwYhakvQUq%2B8nqGFh%2Fo%2B0B0keOoD62RjcPV8xl7wdOmeD3IdyT9peuB9AY5MWdb5cmm2bE0TTEZyN892yc9jUZFnn82iPsx3b8j0P%2F52%2B%2BM1%2FPj7%2Fit%2Bsi8g4PrpVue3zUwMXBiipbgGn008VVE4EOebKzZRLZVoAjF6tBu7ZRry%2B9KpZ4grMy2hEeI4nIZc1Urc41gUlcroFfvg5PsBKyF6HdWuYJoWveu3AUbipZqG69mkQQ6tBFKjCC0Djam0vb1%2B85xK60HtvJ9rHOz3eK5WMpQljzSbAAzTbm4WPX4OgZ7iW%2F5%2FbMXt1X94Bx%2F%2BAg%3D%3D