# Construct Blablahcar

Blablahcar data model concepts, database construction & builds.
[The manual](BBCdeploy_guide.md) was written to make the build process as simple as possible.

![data conditioning flow](data-conditioning_final.png)  
The database construction part is represented by data arrows, two cases on the above image:
- json -> PostgreSQL _(raw input)_
- csv -> PostgreSQL _(restructured input)_